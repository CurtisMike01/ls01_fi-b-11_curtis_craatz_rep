import java.util.Scanner;

public class Noten {

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);

		System.out.println("Bitte geben Sie ihre Note ein: ");
		System.out.println("- Anmerkung: Zur Auswahl steht das standardisierte deutsche Notensystem von 1-6 - ");
		String zahl1 = s.next();


		switch (zahl1) {
		case "1":
		case "a":
		case "A":
		case "eins":
			System.out.println("Sehr gut (Note 1) - Weiter so!");
			break;
			
		case "2":
		case "b":
		case "B":
		case "zwei":
			System.out.println("Gut (Note 2) - Auf dem Weg zur eins, weiter so!");
			break;
			
		case "3":
		case "c":
		case "C":
		case "drei":
			System.out.println("Befriedigend (Note 3) - Du schaffst das beim n�chsten Mal sicher besser!");
			break;
			
		case "4":
		case "d":
		case "D":
		case "vier":
			System.out.println("Ausreichend (Note 4) - Gerade so geschafft, versuch mehr zu �ben!");
			break;
			
		case "5":
		case "E":
		case "e":
		case "f�nf":
			System.out.println("Mangelhaft (Note 5) - Das reicht leider nicht, bitte sieh dir das Thema noch einmal an!");
			break;
			
		case "6":
		case "F":
		case "f":
		case "sechs":
			System.out.println("Ungen�gend (Note 6) - Das wird so nichts, bitte spreche mit deinem Lehrer, damit dieser dich unterst�tzen kann");
			break;
			
		default:
			System.out.println(" ");
			System.out.println("Diese Note gibt es im deutschen Notensystem nicht, bitte vergewissere dich, dass du keine Notenpunkte oder gar andere Notensysteme eingeben hast.");	
			
		}
	}

}
