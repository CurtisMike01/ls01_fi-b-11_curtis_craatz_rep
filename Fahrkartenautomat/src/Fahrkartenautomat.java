﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       float zuZahlenderBetrag ; 
       float rückgabebetrag;
       
    
       zuZahlenderBetrag = fahrkartenbestellungErfassen (tastatur);
       rückgabebetrag = fahrkartenBezahlen (zuZahlenderBetrag, tastatur);
       fahrkartenAusgeben();
       rueckgeldAusgeben (rückgabebetrag);
       tastatur.close();
       
    }
    
    public static float fahrkartenbestellungErfassen (Scanner myScanner) {
    	float preisDesTickets;
    	int zahlTickets; 
    	float gesamt;
    	
       System.out.printf("Hier bitte den Preis des Tickets eingeben (Euro): ");
       preisDesTickets = myScanner.nextFloat();
       
       System.out.printf("Hier bitte die Anzahl der Tickets eingeben?: ");
       zahlTickets = myScanner.nextInt();
       gesamt= preisDesTickets * zahlTickets;
       return gesamt;
    }
    
       
       // Geldeinwurf
       // -----------
       
     public static float fahrkartenBezahlen (float kosten, Scanner myScanner) {
    	
    	 float eingezahlterGesamtbetrag = 0.0f;
    	 float eingeworfeneMünze;
    
       while(eingezahlterGesamtbetrag < kosten) {
    	   System.out.printf("\nFolgender Betrag muss noch gezahlt werden: %.2f Euro", kosten - eingezahlterGesamtbetrag);
    	   System.out.printf("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = myScanner.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
          }
       return eingezahlterGesamtbetrag - kosten; 
     }

       // Fahrscheinausgabe
       // -----------------
     
     public static void fahrkartenAusgeben () {
     
    	 System.out.println("Die Fahrscheine werden gedruckt!");
       
    	 for (int i = 0; i < 23; i++)
       {
          System.out.print("=");
          
          
          try {
			Thread.sleep(180);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
     }

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
     public static void rueckgeldAusgeben (float rückgabebetrag ) {
    	 
       if(rückgabebetrag > 0.0) {
    	   //double e = rückgabebetrag; 
    	   System.out.printf("Ihr Rückgeld in Höhe von %.2f Euro", rückgabebetrag);
    	   System.out.println("\nwird in folgenden Münzen ausgezahlt:");
    	  
    	    
    	   
   
    	   
           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 Euro");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 Euro");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 Cent");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 Cent");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 Cent");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 Cent");
 	          rückgabebetrag -= 0.05;
           }
           while(rückgabebetrag >= 0.02)// 2 CENT-Münzen
           {
 	         System.out.println("2 Cent");
	          rückgabebetrag -= 0.02;
       	   }
           
       	   while(rückgabebetrag >= 0.01)// 1 CENT-Münzen
       	   {
	          System.out.println("1 Cent");
 	          rückgabebetrag -= 0.01;
 	          
           }
       }
       System.out.println("\nVergessen Sie nicht, den/die Fahrschein/e\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt und bleiben Sie gesund!");
       
    }
}