import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		float zuZahlenderBetrag;
		float r�ckgabebetrag;

		zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
		r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
		fahrkartenAusgeben();
		rueckgeldAusgeben(r�ckgabebetrag);
		tastatur.close();

	}

	public static float fahrkartenbestellungErfassen(Scanner myScanner) {
		float preisDesTickets;
		int zahlTickets;
		float gesamt;

		System.out.printf("Hier bitte den Preis des Tickets eingeben (Euro): ");
		preisDesTickets = myScanner.nextFloat();
		if (preisDesTickets <= 0) {
			preisDesTickets = 1;
			System.out.println("Der Preis muss h�her als 0 sein. Der Ticketpreis ist auf 1 gesetzt worden.");
		}


		System.out.printf("Hier bitte die Anzahl der Tickets eingeben?: ");
		zahlTickets = myScanner.nextInt();
		if (zahlTickets < 1 || zahlTickets > 10) {
			zahlTickets = 1;
			System.out.println("Die Anzahl der Tickets wird auf 1 gesetzt, da ihre Eingabe nicht zul�ssig ist.");
		}
		gesamt = preisDesTickets * zahlTickets;
		return gesamt;
	}

	// Geldeinwurf


	public static float fahrkartenBezahlen(float kosten, Scanner myScanner) {

		float eingezahlterGesamtbetrag = 0.0f;
		float eingeworfeneM�nze;

		while (eingezahlterGesamtbetrag < kosten) {
			System.out.printf("\nFolgender Betrag muss noch gezahlt werden: %.2f Euro",
					kosten - eingezahlterGesamtbetrag);
			System.out.printf("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = myScanner.nextFloat();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		return eingezahlterGesamtbetrag - kosten;
	}

	// Fahrscheinausgabe
	

	public static void fahrkartenAusgeben() {

		System.out.println("Die Fahrscheine werden gedruckt!");

		for (int i = 0; i < 23; i++) {
			System.out.print("=");

			try {
				Thread.sleep(180);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	// R�ckgeldberechnung und -Ausgabe

	
	
	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {
			System.out.format("Der R�ckgabebetrag in H�he von %4.2f Euro %n", rueckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) {
				rueckgabebetrag = runden(rueckgabebetrag -= 2.0);
				muenzeAusgeben(2, "EURO");
			}
			while (rueckgabebetrag >= 1.0) {
				rueckgabebetrag = runden(rueckgabebetrag -= 1.0);
				muenzeAusgeben(1, "EURO");
			}
			while (rueckgabebetrag >= 0.5) 
			{
				rueckgabebetrag = runden(rueckgabebetrag -= 0.5);
				muenzeAusgeben(50, "CENT");
			}
			while (rueckgabebetrag >= 0.2) 
			{
				rueckgabebetrag = runden(rueckgabebetrag -= 0.2);
				muenzeAusgeben(20, "CENT");
			}
			while (rueckgabebetrag >= 0.1) 
			{
				rueckgabebetrag = runden(rueckgabebetrag -= 0.1);
				muenzeAusgeben(10, "CENT");
			}
			while (rueckgabebetrag >= 0.05)
			{
				rueckgabebetrag = runden(rueckgabebetrag -= 0.05);
				muenzeAusgeben(5, "CENT");
			}
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println("" + betrag + " " + einheit);
		
		
		
	}
	
	public static double runden(double zahl) {
		zahl = Math.round(zahl * 100)/100.00;
		return zahl;
	
    }

	
	{
		System.out.println("\nVergessen Sie nicht, den/die Fahrschein/e\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt und bleiben Sie gesund!");

	}
}