import java.util.Scanner;

public class Taschenrechner {

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);

		System.out.println("Bitte geben Sie die erste Zahl ein: ");
		double zahl1 = s.nextDouble();

		System.out.println("\nWie sollen die beiden Zahlen verarbeitet werden? Bitte w�hlen Sie einen der folgenden Rechenoperation aus.");
		System.out.println("------------------------------------------------");
		System.out.println("\n(+ oder a) steht f�r Addition");
		System.out.println("(- oder s) steht f�r die Subtraktion");
		System.out.println("(* oder m) steht f�r die Multiplikation");
		System.out.println("(/ oder d) steht f�r Division");
		System.out.println("\n------------------------------------------------");
		System.out.println("\nIhre Eingabe bitte:");
		String rechenoperator = s.next();

		System.out.println("\nBitte geben Sie die zweite Zahl ein:");
		double zahl2 = s.nextDouble();

		double erg = 0;

		switch (rechenoperator) {
		case "+":
		case "a":
			erg = zahl1 + zahl2;
			System.out.println(" ");
			System.out.println(zahl1 + " " + " + " + zahl2 + " = " + erg);
			break;
		case "-":
		case "s":
			System.out.println(" ");
			erg = zahl1 - zahl2;
			System.out.println(zahl1 + " " + " - " + zahl2 + " = " + erg);
			break;
		case "*":
		case "m":
			System.out.println(" ");
			erg = zahl1 * zahl2;
			System.out.println(zahl1 + " " + " * " + zahl2 + " = " + erg);
			break;
		case "/":
		case "d":
			System.out.println(" ");
			erg = zahl1 / zahl2;
			System.out.println(zahl1 + " " + " / " + zahl2 + " = " + erg);
			break;
		default:
			System.out.println(" ");
			System.out.println("Sie haben keiner der obengenannten zul�ssigen Rechenoperatoren ausgew�hlt, versuchen Sie es erneut!");	
			
			
			
		}
	}

}
